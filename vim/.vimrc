set nocompatible

set tabstop=4
set list
set listchars=tab:>-

set hidden          " Allow edit buffers to be hidden.
set history=1000
set undolevels=1000

set foldenable
set foldmethod=syntax
set incsearch
set hlsearch
set showmatch       " Show brackets.

set t_Co=256
colo wombat256mod
syntax on

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
